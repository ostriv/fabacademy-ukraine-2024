# FabAcademy Ukraine 2024


This is the main repo for the FabAcademy 2024 at Platfor Ostriv, Kyiv, Ukraine.


* Dmytro Senchuk [Repo](https://gitlab.com/ssavito1122/ds_fa_ua_2024)

* Diana Shramenko [Repo](https://gitlab.com/dianashramenko12/fabacademy-ukraine)

* Tetiana Solomakha [Website](https://tetiana-solomakha-fabacademy-2024-319b3adf061c629c8a4debeae5380.gitlab.io/#section3) [Repo](https://gitlab.com/tetiana.solomakha/TS_FA_UA_2024)

* Vladyslav Holets [Website](https://vh-fa-ua-2024-vladislavholets-474dddc2516e8fb10b4c61b966143c8be.gitlab.io/)[Repo](https://gitlab.com/vladyslav.holets/VH_FA_UA_2024)

* Iryna Porokhovnychenko

* Vladyslav Bulatov
